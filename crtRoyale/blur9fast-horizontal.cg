/////////////////////////////////  MIT LICENSE  ////////////////////////////////

//  Copyright (C) 2014 TroggleMonkey
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to
//  deal in the Software without restriction, including without limitation the
//  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
//  sell copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.


/////////////////////////////  SETTINGS MANAGEMENT  ////////////////////////////

//  PASS SETTINGS:
//  gamma-management.h needs to know what kind of pipeline we're using and
//  what pass this is in that pipeline.  This will become obsolete if/when we
//  can #define things like this in the .cgp preset file.
//#define GAMMA_ENCODE_EVERY_FBO
//#define FIRST_PASS
//#define LAST_PASS
//#define SIMULATE_CRT_ON_LCD
//#define SIMULATE_GBA_ON_LCD
//#define SIMULATE_LCD_ON_CRT
//#define SIMULATE_GBA_ON_CRT


//////////////////////////////////  INCLUDES  //////////////////////////////////

#include "crtRoyale/gamma-management.h"
#include "crtRoyale/blur-functions.h"

/////////////////////////////////  STRUCTURES  /////////////////////////////////
//  prev: crtRoyale/blur9fast-vertical.cg
//  next: crtRoyale/crt-royale-mask-resize-vertical-temp.cg

namespace pass4 {
    #define HALATION_BLUR pass4

    texture texOut {
        Width  = OUTPUT_SIZE4_X;
        Height = OUTPUT_SIZE4_Y;
    };

    sampler samplerIn  { Texture = pass3::texOut; };
    sampler samplerOut { Texture = pass4::texOut; };

    static const float2 video_size   = float2(OUTPUT_SIZE3_X, OUTPUT_SIZE3_Y);
    static const float2 texture_size = float2(OUTPUT_SIZE3_X, OUTPUT_SIZE3_Y);
    static const float2 output_size  = float2(OUTPUT_SIZE4_X, OUTPUT_SIZE4_Y);

    struct out_vertex {
        float4 position         : POSITION;
        float2 tex_uv           : TEXCOORD0;
        float2 blur_dxdy        : TEXCOORD1;
    };


////////////////////////////////  VERTEX SHADER  ///////////////////////////////

    out_vertex main_vertex ( in uint id : SV_VertexID) {
        input_struct IN = build_input( video_size, texture_size, output_size);

        out_vertex OUT;
        getTexcoord(id, OUT.tex_uv, OUT.position);

        //  Get the uv sample distance between output pixels.  Blurs are not generic
        //  Gaussian resizers, and correct blurs require:
        //  1.) IN.output_size == IN.video_size * 2^m, where m is an integer <= 0.
        //  2.) mipmap_inputN = "true" for this pass in .cgp preset if m != 0
        //  3.) filter_linearN = "true" except for 1x scale nearest neighbor blurs
        //  Gaussian resizers would upsize using the distance between input texels
        //  (not output pixels), but we avoid this and consistently blur at the
        //  destination size.  Otherwise, combining statically calculated weights
        //  with bilinear sample exploitation would result in terrible artifacts.
        const float2 dxdy_scale = IN.video_size/IN.output_size;
        const float2 dxdy = dxdy_scale/IN.texture_size;
        //  This blur is horizontal-only, so zero out the vertical offset:
        OUT.blur_dxdy = float2(dxdy.x, 0.0);

        return OUT;
    }


///////////////////////////////  FRAGMENT SHADER  //////////////////////////////

    float4 main_fragment(in out_vertex VAR) : COLOR {
        input_struct IN = build_input( video_size, texture_size, output_size);
        float3 color = tex2Dblur9fast(samplerIn, VAR.tex_uv, VAR.blur_dxdy);
        //  Encode and output the blurred image:
        return encode_output(float4(color, 1.0));
    }
} //pass4