uniform int FRAME_COUNT < source = "framecount"; >;

#define mask_texture_small_size_x 64
#define mask_texture_small_size_y 64
#define mask_texture_large_size_x 512
#define mask_texture_large_size_y 512

texture mask_grille_texture_small
< source = "crtRoyale/TileableLinearApertureGrille15Wide8And5d5SpacingResizeTo64.png";> {
    Width = mask_texture_small_size_x; Height = mask_texture_small_size_y;
};

texture mask_grille_texture_large
< source = "crtRoyale/TileableLinearApertureGrille15Wide8And5d5Spacing.png";> {
    Width = mask_texture_large_size_x; Height = mask_texture_large_size_y; MipLevels = 6;
};

texture mask_slot_texture_small
< source = "crtRoyale/TileableLinearSlotMaskTall15Wide9And4d5Horizontal9d14VerticalSpacingResizeTo64.png";> {
    Width = mask_texture_small_size_x; Height = mask_texture_small_size_y;
};

texture mask_slot_texture_large
< source = "crtRoyale/TileableLinearSlotMaskTall15Wide9And4d5Horizontal9d14VerticalSpacing.png";> {
    Width = mask_texture_large_size_x; Height = mask_texture_large_size_y; MipLevels = 6;
};

texture mask_shadow_texture_small
< source = "crtRoyale/TileableLinearShadowMaskEDPResizeTo64.png";> {
    Width = mask_texture_small_size_x; Height = mask_texture_small_size_y;
};

texture mask_shadow_texture_large
< source = "crtRoyale/TileableLinearShadowMaskEDP.png";> {
    Width = mask_texture_large_size_x; Height = mask_texture_large_size_y; MipLevels = 6;
};

sampler mask_grille_sampler_small { Texture = mask_grille_texture_small; AddressU = REPEAT; AddressV = REPEAT; };
sampler mask_grille_sampler_large { Texture = mask_grille_texture_large; AddressU = REPEAT; AddressV = REPEAT; };

sampler mask_slot_sampler_small { Texture = mask_slot_texture_small; AddressU = REPEAT; AddressV = REPEAT; };
sampler mask_slot_sampler_large { Texture = mask_slot_texture_large; AddressU = REPEAT; AddressV = REPEAT; };

sampler mask_shadow_sampler_small { Texture = mask_shadow_texture_small; AddressU = REPEAT; AddressV = REPEAT; };
sampler mask_shadow_sampler_large { Texture = mask_shadow_texture_large; AddressU = REPEAT; AddressV = REPEAT; };

/**********************************************
 *
 * cg to reshadeFX adapters
 *
 **********************************************/

#define inline  //no inline in Reshade (or in hlsl)

#define fmod(a, b) ( a - b * floor(a/b))

//#define fmod(a, b) (a % b)
#define _stitch(a, b) a ## b
#define stitch(a, b) _stitch(a, b)

void getTexcoord( uint id, out float2 txcoord, out float4 pos ) {
    txcoord.x = (id == 2) ? 2.0 : 0.0;
	txcoord.y = (id == 1) ? 2.0 : 0.0;
    pos     = float4(txcoord * float2(2.0, -2.0) + float2(-1.0, 1.0), 0.0, 1.0);
}

struct float4x3_type {
    float3 row0, row1, row2, row3;
};

float4x3_type float4x3(float3 row0, float3 row1, float3 row2, float3 row3){
    float4x3_type o;
    o.row0 = row0;
    o.row1 = row1;
    o.row2 = row2;
    o.row3 = row3;
    return o;
}

struct input_struct {
    float2 video_size;
    float2 texture_size;
    float2 output_size;
    float frame_count;
    float frame_direction;
    float frame_rotation;
};

input_struct build_input(   const float2 video_size, const float2 texture_size,
                            const float2 output_size ) {
    input_struct o;
        o.video_size        = video_size;
        o.texture_size      = texture_size;
        o.output_size       = output_size;
        o.frame_count       = FRAME_COUNT;
        o.frame_direction   = 1.0;
        o.frame_rotation    = 0.0;
    return o;
}

// orig_struct build_orig( const float2 video_size, const float2 texture_size,
//                         const float2 output_size, const sampler2D samplerIn ) {
//     orig_struct o;
//         o.video_size   = video_size;
//         o.texture_size = texture_size;
//         o.output_size  = output_size;
//         o.samplerIn    = samplerIn;
//     return o;
// }

float3 mul4_4x3(float4 vec, float4x3_type m) {
    return float3( dot(vec, float4(m.row0.x, m.row1.x, m.row2.x, m.row3.x)),
                   dot(vec, float4(m.row0.y, m.row1.y, m.row2.y, m.row3.y)),
                   dot(vec, float4(m.row0.z, m.row1.z, m.row2.z, m.row3.z)));
}

//liberto note:
//
//video_size   == output_size from last pass
//texture_size == pow2(ceil(log2(video_size)))
//output_size  == output size from .cgp

//here:
//video size == texture size
//           == output_size from last pass
//also notes:

#define EMULATE_SRC_SIZE_X      320.0
#define EMULATE_SRC_SIZE_Y      240.0

#define BLOOM_APPROX_SIZE_X     320.0
#define BLOOM_APPROX_SIZE_Y     240.0

#define MASK_RESIZE_VIEWPORT_SCALE_X 0.0625
#define MASK_RESIZE_VIEWPORT_SCALE_Y 0.0625

texture texColorBuffer : COLOR;

//////////////////////////////  prepass  /////////////////////////////
//
//  create mipmapped src texture
//

#define OUTPUT_SIZE_PREPASS_X 0.5*BUFFER_WIDTH
#define OUTPUT_SIZE_PREPASS_Y 0.5*BUFFER_HEIGHT

// texture texTest < source = "image.jpg"; > {
//         Width     = OUTPUT_SIZE_PREPASS_X;
//         Height    = OUTPUT_SIZE_PREPASS_Y;
//     };

namespace prepass {
    sampler samplerIn {
        Texture = texColorBuffer;
    };

    texture texOut {
        Width     = EMULATE_SRC_SIZE_X;
        Height    = EMULATE_SRC_SIZE_Y;
        MipLevels = 5;
    };

    struct out_vertex {
        float4 position : POSITION;
        float2 txcoord  : TEXCOORD;
    };

    out_vertex main_vertex( in uint ID : SV_VertexID ) {
        out_vertex o;
        getTexcoord(ID, o.txcoord, o.position);
        return o;
    }

    float4 main_fragment( in out_vertex i) : COLOR {
        return tex2D( samplerIn, i.txcoord);
    }

} //prepass

/////////////////////////////////  P0  /////////////////////////////////
//      Linearize the input based on CRT gamma and bob interlaced fields.
// (Bobbing ensures we can immediately blur without getting artifacts.)
//
#define OUTPUT_SIZE0_X  EMULATE_SRC_SIZE_X
#define OUTPUT_SIZE0_Y  EMULATE_SRC_SIZE_Y

#include "crtRoyale/crt-royale-first-pass-linearize-crt-gamma-bob-fields.cg"

/////////////////////////////////  P1  /////////////////////////////////
//      Resample interlaced (and misconverged) scanlines vertically.
// Separating vertical/horizontal scanline sampling is faster: It lets us
// consider more scanlines while calculating weights for fewer pixels, and
// it reduces our samples from vertical*horizontal to vertical+horizontal.
// This has to come right after ORIG_LINEARIZED, because there's no
// "original_source" scale_type we can use later.
//
#define OUTPUT_SIZE1_X OUTPUT_SIZE0_X
#define OUTPUT_SIZE1_Y BUFFER_HEIGHT

#include "crtRoyale/crt-royale-scanlines-vertical-interlacing.cg"

/////////////////////////////////  P2  /////////////////////////////////
//      Do a small resize blur of ORIG_LINEARIZED at an absolute size, and
// account for convergence offsets.  We want to blur a predictable portion of the
// screen to match the phosphor bloom, and absolute scale works best for
// reliable results with a fixed-size bloom.  Picking a scale is tricky:
// a.) 400x300 is a good compromise for the "fake-bloom" version: It's low enough
//     to blur high-res/interlaced sources but high enough that resampling
//     doesn't smear low-res sources too much.
// b.) 320x240 works well for the "real bloom" version: It's 1-1.5% faster, and
//     the only noticeable visual difference is a larger halation spread (which
//     may be a good thing for people who like to crank it up).
// Note the 4:3 aspect ratio assumes the input has cropped geom_overscan (so it's
// *intended* for an ~4:3 aspect ratio).
//
#define OUTPUT_SIZE2_X BLOOM_APPROX_SIZE_X
#define OUTPUT_SIZE2_Y BLOOM_APPROX_SIZE_Y

#include "crtRoyale/crt-royale-bloom-approx.cg"

/////////////////////////////////  P3  /////////////////////////////////
//      Vertically blur the input for halation and refractive diffusion.
// Base this on BLOOM_APPROX: This blur should be small and fast, and blurring
// a constant portion of the screen is probably physically correct if the
// viewport resolution is proportional to the simulated CRT size.
//
#define OUTPUT_SIZE3_X OUTPUT_SIZE2_X
#define OUTPUT_SIZE3_Y OUTPUT_SIZE2_Y

#include "crtRoyale/blur9fast-vertical.cg"

/////////////////////////////////  P4  /////////////////////////////////
//      Horizontally blur the input for halation and refractive diffusion.
// Note: Using a one-pass 9x9 blur is about 1% slower.
//
#define OUTPUT_SIZE4_X OUTPUT_SIZE3_X
#define OUTPUT_SIZE4_Y OUTPUT_SIZE3_Y

#include "crtRoyale/blur9fast-horizontal.cg"

/////////////////////////////////  P5  /////////////////////////////////
//       Lanczos-resize the phosphor mask vertically.  Set the absolute
// scale_x5 == mask_texture_small_size.x (see IMPORTANT above).  Larger scales
// will blur, and smaller scales could get nasty.  The vertical size must be
// based on the viewport size and calculated carefully to avoid artifacts later.
// First calculate the minimum number of mask tiles we need to draw.
// Since curvature is computed after the scanline masking pass:
//   num_resized_mask_tiles = 2.0;
// If curvature were computed in the scanline masking pass (it's not):
//   max_mask_texel_border = ~3.0 * (1/3.0 + 4.0*sqrt(2.0) + 0.5 + 1.0);
//   max_mask_tile_border = max_mask_texel_border/
//       (min_resized_phosphor_triad_size * mask_triads_per_tile);
//   num_resized_mask_tiles = max(2.0, 1.0 + max_mask_tile_border * 2.0);
//   At typical values (triad_size >= 2.0, mask_triads_per_tile == 8):
//       num_resized_mask_tiles = ~3.8
// Triad sizes are given in horizontal terms, so we need geom_max_aspect_ratio
// to relate them to vertical resolution.  The widest we expect is:
//   geom_max_aspect_ratio = 4.0/3.0  # Note: Shader passes need to know this!
// The fewer triads we tile across the screen, the larger each triad will be as a
// fraction of the viewport size, and the larger scale_y5 must be to draw a full
// num_resized_mask_tiles.  Therefore, we must decide the smallest number of
// triads we'll guarantee can be displayed on screen.  We'll set this according
// to 3-pixel triads at 768p resolution (the lowest anyone's likely to use):
//   min_allowed_viewport_triads = 768.0*geom_max_aspect_ratio / 3.0 = 341.333333
// Now calculate the viewport scale that ensures we can draw resized_mask_tiles:
//   min_scale_x = resized_mask_tiles * mask_triads_per_tile /
//       min_allowed_viewport_triads
//   scale_y5 = geom_max_aspect_ratio * min_scale_x
//   # Some code might depend on equal scales:
//   scale_x6 = scale_y5
// Given our default geom_max_aspect_ratio and min_allowed_viewport_triads:
//   scale_y5 = 4.0/3.0 * 2.0/(341.33333 / 8.0) = 0.0625
// IMPORTANT: The scales MUST be calculated in this way.  If you wish to change
// geom_max_aspect_ratio, update that constant in user-cgp-constants.h!
//
//#srgb_framebuffer5 = "false" # mask_texture is already assumed linear

#define OUTPUT_SIZE5_X 64
#define OUTPUT_SIZE5_Y MASK_RESIZE_VIEWPORT_SCALE_Y*BUFFER_HEIGHT //Safe for >= 341.333 horizontal triads at viewport size

#include "crtRoyale/crt-royale-mask-resize-vertical-temp.cg"

/////////////////////////////////  P6  /////////////////////////////////
//      Lanczos-resize the phosphor mask horizontally.  scale_x6 = scale_y5.
// TODO: Check again if the shaders actually require equal scales.
//
//#srgb_framebuffer5 = "false" # mask_texture is already assumed linear

#define OUTPUT_SIZE6_X MASK_RESIZE_VIEWPORT_SCALE_X*BUFFER_WIDTH
#define OUTPUT_SIZE6_Y OUTPUT_SIZE5_Y//Safe for >= 341.333 horizontal triads at viewport size

#include "crtRoyale/crt-royale-mask-resize-horizontal.cg"

/////////////////////////////////  P7  /////////////////////////////////
//      Resample (misconverged) scanlines horizontally, apply halation, and
// apply the phosphor mask.
//
//srgb_framebuffer7 = "true"

#define OUTPUT_SIZE7_X BUFFER_WIDTH
#define OUTPUT_SIZE7_Y BUFFER_HEIGHT

#include "crtRoyale/crt-royale-scanlines-horizontal-apply-mask.cg"

/////////////////////////////////  P8  /////////////////////////////////
//      Compute a brightpass.  This will require reading the final mask.
//
//srgb_framebuffer8 = "true"

#define OUTPUT_SIZE8_X BUFFER_WIDTH
#define OUTPUT_SIZE8_Y BUFFER_HEIGHT

#include "crtRoyale/crt-royale-brightpass.cg"

/////////////////////////////////  P9  /////////////////////////////////
//      Blur the brightpass vertically
//
//srgb_framebuffer9 = "true"

#define OUTPUT_SIZE9_X BUFFER_WIDTH
#define OUTPUT_SIZE9_Y BUFFER_HEIGHT

#include "crtRoyale/crt-royale-bloom-vertical.cg"

/////////////////////////////////  P10  /////////////////////////////////
//      Blur the brightpass horizontally and combine it with the dimpass:
//
//srgb_framebuffer10 = "true"

#define OUTPUT_SIZE10_X BUFFER_WIDTH
#define OUTPUT_SIZE10_Y BUFFER_HEIGHT

#include "crtRoyale/crt-royale-bloom-horizontal-reconstitute.cg"

/////////////////////////////////  P11  /////////////////////////////////
//      Compute curvature/AA:
//
//  texture_wrap_mode11 = "clamp_to_edge"

#define OUTPUT_SIZE11_X BUFFER_WIDTH
#define OUTPUT_SIZE11_Y BUFFER_HEIGHT

#include "crtRoyale/crt-royale-geometry-aa-last-pass.cg"

/********************************************************************************************
 *
 * techniques
 *
 ********************************************************************************************/

technique crtRoyale
{
    pass prepass {
        VertexShader = prepass::main_vertex;
        PixelShader  = prepass::main_fragment;
        RenderTarget = prepass::texOut;
    }

    //texture name sharing requires unique names (even under namespaces).
	pass p0  { VertexShader = pass0::main_vertex;  PixelShader = pass0::main_fragment; RenderTarget = pass0::texOut; } //ORIG_LINEARIZED
    pass p1  { VertexShader = pass1::main_vertex;  PixelShader = pass1::main_fragment; RenderTarget = pass1::texOut; } //VERTICAL_SCANLINES
    pass p2  { VertexShader = pass2::main_vertex;  PixelShader = pass2::main_fragment; RenderTarget = pass2::texOut; } //BLOOM_APPROX
    pass p3  { VertexShader = pass3::main_vertex;  PixelShader = pass3::main_fragment; RenderTarget = pass3::texOut; }
    pass p4  { VertexShader = pass4::main_vertex;  PixelShader = pass4::main_fragment; RenderTarget = pass4::texOut; } //HALATION_BLUR
    pass p5  { VertexShader = pass5::main_vertex;  PixelShader = pass5::main_fragment; RenderTarget = pass5::texOut; }
    pass p6  { VertexShader = pass6::main_vertex;  PixelShader = pass6::main_fragment; RenderTarget = pass6::texOut; } //MASK_RESIZE
    pass p7  { VertexShader = pass7::main_vertex;  PixelShader = pass7::main_fragment; RenderTarget = pass7::texOut; } //MASKED_SCANLINES
    pass p8  { VertexShader = pass8::main_vertex;  PixelShader = pass8::main_fragment; RenderTarget = pass8::texOut; } //BRIGHTPASS
    pass p9  { VertexShader = pass9::main_vertex;  PixelShader = pass9::main_fragment;  }
    pass p10 { VertexShader = pass10::main_vertex; PixelShader = pass10::main_fragment; }
    pass p11 { VertexShader = pass11::main_vertex; PixelShader = pass11::main_fragment; }
}

/********************************************************************************************
 *
 * debug
 *
 ********************************************************************************************/

uniform float testvalue = 1;

namespace debug {
    void main_vertex( in  uint   id       : SV_VertexID,
                      out float4 position : POSITION,
                      out float2 txcoord  : TEXCOORD ) {
        getTexcoord(id, txcoord, position);
    }
    float4 main_fragment( float4 pos : POSITION,
                          float2 uv  : TEXCOORD) : COLOR {
        return tex2D( BRIGHTPASS::samplerOut, uv) * testvalue; //orig
    }
}

technique crtRoyale_debug {
    pass p0 { VertexShader = debug::main_vertex;  PixelShader = debug::main_fragment;  }
}

